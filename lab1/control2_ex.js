//
// investigating javascript control structures, functions
//


//
// exercise 1
//
// Create a function named "square" that returns the square of a number.
// e.g.:
// console.log(square(4));
// will print:
// 16


//
// exercise 2
//
// Create a "bubble sort" function that takes an array, and returns
// a sorted array.
// See https://en.wikipedia.org/wiki/Bubble_sort


//
// exercise 3
//
// Use `map` to create a list of objects setting each value to 1.
// input: ['a','b','c']
// output: [{a:1},{b:1},{c:1}]
let input = ['one', 'two', 'three']
