//
// investigating javascript control structures, if & for
//
// The W3 schools is a good source for basic JS information:
// https://www.w3schools.com/js/default.asp
// 

//
// variable declarations
//
// https://www.w3schools.com/js/js_variables.asp

let value = true;
let anumber = 15;

//
// operators
//
// https://www.w3schools.com/js/js_operators.asp
console.log(anumber + 1);
console.log(anumber * 2);
console.log(anumber - 1);
console.log(anumber / 2);
console.log(anumber % 2); // <- modulus


//
// objects
//
// https://www.w3schools.com/js/js_objects.asp

let anobject = {
    make: "Mercury",
    model: "Mountaineer",
    year: 2009
}

//
// lists
//
let alist = ['a', 'x', 'z'];
console.log("alist has length " + alist.length);

//
// if statements
//
// https://www.w3schools.com/js/js_if_else.asp
if (value) {
    console.log("value is TRUE " + value);
} else {
    console.log("value is FALSE " + value);
}

if (anumber > 10) {
    console.log("anumber is > 10 and equals " + anumber);
}

//
// for
//
// https://www.w3schools.com/js/js_loop_for.asp

let i = 0;
for (i = 0; i < 10; i++) {
    console.log(i);
}

// for/in
// iterates over the keys of an object
for (let k in anobject) {
    console.log(k + " = " + anobject[k]);
}

// for/of
// iterate over an array or list
for (let o of alist) {
    console.log(o);
}
