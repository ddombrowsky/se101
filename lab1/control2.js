//
// investigating javascript control structures, functions
//
// https://www.w3schools.com/js/js_functions.asp

//
// standard function with parameters
//
function f1(a, b) {
    console.log("f1: " + a + " " + b);
    return a+b;
}
console.log("return of f1: " + f1(1,2));

//
// using the function as a parameter
//
function f2(e) {
    return e(2,3);
}
console.log("return of f2: " + f2(f1)); // <- note this is NOT f2(f1())
// f2(f1()); <- this will throw an error

//
// anonymous and named functions
//
let a = [2, 4, 6, 8];
function f3(e) {
    console.log("f3: " + (e*2));
}
// named:
a.forEach(f3)

// see also https://www.w3schools.com/jsref/jsref_foreach.asp
// anon:
a.forEach((el,index) => {
    console.log("anon: " + el + " index:" + index);
});
// same thing, different (older) syntax:
//a.forEach(function (el,index) {
//    console.log("anon: " + el + " index:" + index);
//});

// "map" is similar to foreach, but does not change the 
// original array and returns an array of the result elements.
// https://www.w3schools.com/jsref/jsref_map.asp
//
// note the simplified syntax:
b = a.map(el => el * el);
console.log('using map:');
console.log(b);

// see also https://www.w3schools.com/jsref/jsref_sort.asp
a.sort((a,b) => {
    // will sort in reverse order
    return b - a;
});
console.log(a.join(' '));



//
// recursion
//
// Functions can be recursive, but be careful!
//
function f4(e) {
    if (e>0) {
        f4(e - 1);
    }
    console.log(e);
}
f4(5);
