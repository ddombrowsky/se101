//
// investigating javascript control structures, if & for
//


// exercise 1
//
// Write a loop that will output the numbers 1 through 10,
// printing if the number is even or odd.
//
// example output:
// 1 odd
// 2 even
// 3 odd
// 4 even
// ... and so on ...


// exercise 2
// 
// Write a loop that will print the numbers 10 to 1 
// in that order.  The array range is already defined.
//
// example output:
// 10
// 9
// 8
// 7
// ... and so on ...
let numbers = [1,2,3,4,5,6,7,8,9,10];
